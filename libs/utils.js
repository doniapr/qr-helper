
export const getTagValue = data => {
    let resp = new Map()
    let i = 0;
    while (i < data.length) {
        let tag = data.substring(i, i + 2);
        i += 2;
        let valueLength = Number(data.substring(i, i + 2));
        i += 2;
        let value = data.substring(i, i + valueLength);
        i += valueLength;
        resp.set(tag, value)
    }

    return resp
}


function luhn_checksum(code) {
    var len = code.length
    var parity = len % 2
    var sum = 0
    for (var i = len-1; i >= 0; i--) {
        var d = parseInt(code.charAt(i))
        if (i % 2 == parity) { d *= 2 }
        if (d > 9) { d -= 9 }
        sum += d
    }
    return sum % 10
}

/* luhn_calculate
 * Return a full code (including check digit), from the specified partial code (without check digit).
 */
export const luhnCalculate = partcode => {
    var checksum = luhn_checksum(partcode + "0")
    return checksum == 0 ? 0 : 10 - checksum
}