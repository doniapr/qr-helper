import express from "express";
import {parseMPM, parseTcico} from "../controllers/parserController.js";

const router = express.Router();

router.post('/tcico', parseTcico);
router.post('/mpm', parseMPM);

export default router;