import {getTagValue, luhnCalculate} from '../libs/utils.js'

export const parseTcico = (req, res) => {
    let qris = req.body.data
    let parsed = getTagValue(qris)

    let tag40 = parsed.get("40")
    let parsedTag40 = getTagValue(tag40)
    let pan = parsedTag40.get("01")
    let trxPan = pan + luhnCalculate(pan)
    let toAccount = parsedTag40.get("02")

    let merchantType = parsed.get("52")
    let currCode = parsed.get("53")
    let amount = parsed.get("54")
    let trxAmount = "1000000"
    if (amount !== undefined) {
        trxAmount = (amount*100).toString()
    }

    let tag59 = parsed.get("59")
    let tag60 = parsed.get("60")
    let merchantNameLocation = tag59+tag60+"ID"

    let tag61 = parsed.get("61")
    let tag62 = parsed.get("62")
    let addDataNational = "61"+formatTwoChar(tag61.length)+tag61+"62"+formatTwoChar(tag62.length)+tag62


    let date = new Date(new Date().toLocaleString('en', {timeZone: 'Asia/Jakarta'}))
    let trxTime = "" + formatTwoChar(date.getHours()) + formatTwoChar(date.getMinutes()) + formatTwoChar(date.getSeconds())
    let trxDate = "" + formatTwoChar(date.getMonth()+1) + formatTwoChar(date.getDate())

    let dataRes = {
        "trxPAN" : trxPan,
        "transmissionDateTime": ""+ trxDate+trxTime,
        "trxTime": trxTime,
        "trxDate": trxDate,
        "trxAmount": trxAmount.padStart(12, "0"),
        "settlementDate": trxDate,
        "captureDate": trxDate,
        "merchantType": merchantType,
        "trxCurrencyCode": currCode,
        "merchantNameLocation": merchantNameLocation.padEnd(40, " "),
        "additionalDataNational": addDataNational,
        "toAccount": toAccount,
    }

    res.status(200).send({
        code:200,
        message: 'success',
        data: dataRes
    })
}

export const parseMPM = (req, res) => {
    let qris = req.body.data
    let parsed = getTagValue(qris)

    let tag26 = parsed.get("26")
    let parsedTag26 = getTagValue(tag26)
    let pan = parsedTag26.get("01")
    let trxPan = pan + luhnCalculate(pan)
    let toAccount = parsedTag26.get("02")

    let merchantType = parsed.get("52")
    let currCode = parsed.get("53")
    let amount = parsed.get("54")
    let trxAmount = "1000000"
    if (amount !== undefined) {
        trxAmount = (amount*100).toString()
    }

    let tag59 = parsed.get("59")
    let tag60 = parsed.get("60")
    let merchantNameLocation = tag59+tag60+"ID"

    let tag61 = parsed.get("61")
    let tag62 = parsed.get("62")
    let addDataNational = "61"+formatTwoChar(tag61.length)+tag61+"62"+formatTwoChar(tag62.length)+tag62


    let date = new Date(new Date().toLocaleString('en', {timeZone: 'Asia/Jakarta'}))
    let trxTime = "" + formatTwoChar(date.getHours()) + formatTwoChar(date.getMinutes()) + formatTwoChar(date.getSeconds())
    let trxDate = "" + formatTwoChar(date.getMonth()+1) + formatTwoChar(date.getDate())

    let dataRes = {
        "trxPAN" : trxPan,
        "transmissionDateTime": ""+ trxDate+trxTime,
        "trxTime": trxTime,
        "trxDate": trxDate,
        "trxAmount": trxAmount.padStart(12, "0"),
        "settlementDate": trxDate,
        "captureDate": trxDate,
        "merchantType": merchantType,
        "trxCurrencyCode": currCode,
        "merchantNameLocation": merchantNameLocation.padEnd(40, " "),
        "additionalDataNational": addDataNational,
        "toAccount": toAccount,
    }

    res.status(200).send({
        code:200,
        message: 'success',
        data: dataRes
    })
}

function formatTwoChar(str) {
    if (str < 10) {
        return "0" + str
    }

    return str
}
